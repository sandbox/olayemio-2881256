/*global Galleria google */
var global = {};
$ = jQuery;

$(document).ready(function($) {
  if (typeof Galleria != 'undefined') {
    Galleria.loadTheme(Drupal.settings.picasa.path_galleria);
  }
    
  // JQuery objects
  $elem_message = $('#galleria_message');
  $elem_map_canvas = $('#map_canvas');
  $elem_galleria = $elem_message.parent();

  // Javascript objects
  var markers = []; // cache the gallery
  var map = null;
  var markerClusterer = null;
  var imgfolder = Drupal.settings.picasa.path_img;
  var photos_location = Drupal.settings.picasa.photos_by_location;

  /** 
   * Creates the Google Maps used on the site.
   */
  global.initMap = function initialize_map() {
    // Let the user know that the canvas is still loading
    $elem_message.show().text("Loading...");
  
    // Initialize the style for the Google Maps. 
    // Create an array of styles. Turn off road labels
    var styleOff = [{
        visibility: 'off'
    }];
    var stylesArray = [{
        featureType: 'administrative.neighborhood',
        stylers: styleOff
    }, {
        featureType: 'administrative.land_parcel',
        stylers: styleOff
    }, {
        featureType: 'poi',
        stylers: styleOff
    }, {
        featureType: 'landscape',
        stylers: styleOff
    }, {
        featureType: 'road',
        stylers: styleOff
    }];


    // Lat and Long of Ife, which will be the center of the map
    var latlngIfe = new google.maps.LatLng(7.528836, 4.535766);

    // Create a new StyledMapType object, passing it the array of styles,
    // as well as the name to be displayed on the map type control.
    var styledMap = new google.maps.StyledMapType(stylesArray, {
        name: 'City View'
    });
    // Create a map object, and include the MapTypeId to add
    // to the map type control.
    // Specify the Map Options
    var mapOptions = {
      scrollWheel: false,
      minZoom: 2,
      zoom: 2,
      //draggable: false,
      mapTypeControl: false,
      streetViewControl: false,
      rotateControl: false,
      center: latlngIfe,
      mapTypeId: 'city_view'
    };
    map = new google.maps.Map($elem_map_canvas[0], mapOptions);
    
    // Add Event Listener for Resize Events
    // Resize stuff...
    google.maps.event.addDomListener(window, "resize", function() {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlngIfe); 
    });

    // Associate the styled map with the MapTypeId and set it to display.
    map.mapTypes.set('city_view', styledMap);
    map.setMapTypeId('city_view');
    getMarkers();
    
    // Hide the wait message
    $elem_message.hide().text("").fadeOut(1000);
  };

  /** 
   * Creates markers to add to the maps. A marker is added to the map 
   * for each location of photos.
   * Markers in similar areas are also clustered together using the
   * MarkerClusterer Javascript library for Google Maps.
   */
  function getMarkers() {
    
    // For each location of photos and add a marker to the map
    for (i = 0; i < photos_location.length; i++){
      var name = photos_location[i].data['name'];
      var latlng = photos_location[i].data['latlng'].split(',');
      markers.push(addMarker(new google.maps.LatLng(latlng[0], latlng[1]), name));
    }
    
    // Create a marker cluster for markers that are close to each other
   /* markerClusterer = new MarkerClusterer(map, markers, {
      maxZoom: 14,
      gridSize: 40,
      styles: null,
    });*/
    markerClusterer = new MarkerClusterer(map, markers, {
      imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      
  }
  
  /**
   * Helper function to add individual markers to a map
   * @param {number[]} location - A latitude, longitude coordinate
   * @param {number} location.lat - The latitude of the location
   * @param {number} location.lng - The longitude of the location
   * @param {string} title - The text to show with the location
   * @returns {Object} marker - A Google Maps marker object corresponding to the location
   */
  function addMarker(location, title) {
    var image = new google.maps.MarkerImage(imgfolder.concat('google_maps_marker_image.png'), new google.maps.Size(64, 64), new google.maps.Point(0, 0), new google.maps.Point(32, 64));
    var shadow = new google.maps.MarkerImage(imgfolder.concat('google_maps_marker_image_shadow.png'), new google.maps.Size(100, 64), new google.maps.Point(0, 0), new google.maps.Point(32, 64));
    var shape = {
        coord: [39, 7, 40, 8, 40, 9, 41, 10, 41, 11, 41, 12, 42, 13, 42, 14, 43, 15, 57, 16, 58, 17, 58, 18, 58, 19, 58, 20, 58, 21, 58, 22, 58, 23, 58, 24, 58, 25, 58, 26, 58, 27, 58, 28, 58, 29, 58, 30, 58, 31, 58, 32, 58, 33, 58, 34, 58, 35, 58, 36, 58, 37, 58, 38, 58, 39, 58, 40, 58, 41, 58, 42, 58, 43, 58, 44, 58, 45, 58, 46, 58, 47, 58, 48, 58, 49, 58, 50, 58, 51, 58, 52, 58, 53, 58, 54, 58, 55, 58, 56, 5, 56, 5, 55, 5, 54, 5, 53, 5, 52, 5, 51, 5, 50, 5, 49, 5, 48, 5, 47, 5, 46, 5, 45, 5, 44, 5, 43, 5, 42, 5, 41, 5, 40, 5, 39, 5, 38, 5, 37, 5, 36, 5, 35, 5, 34, 5, 33, 5, 32, 5, 31, 5, 30, 5, 29, 5, 28, 5, 27, 5, 26, 5, 25, 5, 24, 5, 23, 5, 22, 5, 21, 5, 20, 5, 19, 5, 18, 5, 17, 6, 16, 7, 15, 7, 14, 7, 13, 8, 12, 22, 11, 22, 10, 23, 9, 23, 8, 24, 7, 39, 7],
        type: 'poly'
    };
    var marker = new google.maps.Marker({
        draggable: false,
        raiseOnDrag: false,
        title: title,
        position: location,
        map: map
    });
    google.maps.event.addListener(marker, 'click', function() {
      var location = marker.getTitle();
      dataLayer.push({
        'event' : 'Generic Interaction',
        'eventCategory' : 'Google Maps Marker',
        'eventAction' : 'Launch Gallery',
        'eventLabel' : location,
        'eventValue' : undefined
      });
      launchPhotoGallery(location);
    });
    return marker;
  }
  
  /**
   *
   * @callback sort_func
   * @param {*} a - The first item to be compared
   * @param {*} b - The second item to be compared
   * @returns Whether a is greater than b
   */
  /**
   * Sort an HTML list.
   * @param {number[} ul - An HTML list
   * @callback {function[]} sort_func - The sort function to use
   * @param {number[]} sortDescending - Whether to sort ascending or descending (true)
   */
  function sortList(ul, sort_func, sortDescending){
    
    var new_ul = ul.cloneNode(false);

    // Add all lis to an array
    var lis = [];
    for(var i = ul.childNodes.length; i--;){
      if(ul.childNodes[i].nodeName === 'LI') {
        lis.push(ul.childNodes[i]);
      }
    }

    // Sort the list
    lis.sort(sort_func);
    
    // In descending order if necessary    
    if(sortDescending){
      lis.reverse();
    }

    // Add them into the ul in order
    for(var i = 0; i < lis.length; i++){
      new_ul.appendChild(lis[i]);
    }
    
    ul.parentNode.replaceChild(new_ul, ul);
  }
  
  /**
   * Launches the photo gallery based on user actions
   *
   * @param {String} location - The location that will be displayed
   */
  function launchPhotoGallery(location){
    $.getJSON("picasa/output/location_photos/" + location, function(photos) {
      if (photos === null) {
        $elem_message.show().text("Sorry, I am having trouble showing my pictures from " + location,+ ". Please select another city.").fadeOut(10000, function() {
            $elem_message.text("");
        });
        return;
      }
      // Modify the image title to include the location
      for (var i = 0; i < photos.results.length; i++) {
        photos.results[i].title = location;
      }
      $('.galleria').createGalleria({
                  data: photos.results,
                  limit: 50
              });
    });
  }
  
  /**
   * jQuery functions
   */
  /**
   * Helper function to run and open the Galleria
   */
  $.fn.createGalleria = function() {
    setTimeout(4000);
    // load the theme

    // Grab the arguments and create variables
    var $this = $(this)[0];
    var args = arguments[0] || {};
    var data = args.data;
    var limit = args.limit;
    var data_size = data.length;
    var batch = 0;

    // Create a new galleria tag
    var elem_galleria = $('<div>', {
        id: 'galleria'
    }).prependTo($this);

    // add loader text and show
    // create and append the loader growl
    var elem_loader = $('<div>', {
        id: 'loader'
    }).appendTo('#galleria');
    elem_loader.text('Loading ' + $(this).text()).show();

    // hide the loader
    elem_loader.fadeOut('fast');

    var xOffset = window.pageXOffset;
    var yOffset = window.pageYOffset;
    elem_galleria.galleria({
      autoplay: true,
      imageCrop: false,
      fullscreenCrop: false,
      lightbox: false,
      swipe: true,
      debug: true,
      responsive:true, 
      height:0.5,
      thumbnails: 'lazy',
      showInfo: true,
      transition: 'fade',
      transition_speed: 600,
      dataSource: data.slice(batch++ * limit, batch * limit),
      trueFullscreen: false,
      // add the data as dataSource
      extend: function(options) {
        var $galleria = this;
        this.lazyLoadChunks(10, 200);
        // Add a close button. When the galleria opens, launch it. When the galleria goes away, destroy it
        this.addElement('exit').appendChild('container','exit');
        
        var btn = this.$('exit').hide().text('close (press "Esc")').click(function(e) {
          $galleria.exitFullscreen();
          dataLayer.push({
            'event' : 'Generic Interaction',
            'eventCategory' : 'Galleria Events',
            'eventAction' : 'Close Gallery',
            'eventLabel' : 'Click',
            'eventValue' : undefined
          });
        });
        this.bind('fullscreen_enter', function() {
          btn.show();
        });

        $(window).scrollTop();
        $galleria.enterFullscreen();
        
        this.$('thumb-nav-right').click(function(e) {
          $('.galleria-total').text(data_size);
        });
        this.$('thumb-nav-left').click(function(e) {
          $('.galleria-total').text(data_size);
        });
        this.bind('loadstart', function(e) {
          if ((e.index + 1) === batch * limit && data_size > batch * limit) {
            $galleria.push(data.slice(batch++ * limit, batch * limit));
          }
          $('.galleria-total').text(data_size);
        });
        
        

        // When the user exits, close the galleria completely.
        this.bind('fullscreen_exit', function(e) {
          $galleria.destroy();
          elem_galleria.remove();
          window.scrollTo(xOffset, yOffset);
          console.log(e);
          dataLayer.push({
            'event' : 'Generic Interaction',
            'eventCategory' : 'Galleria Events',
            'eventAction' : 'Close Gallery',
            'eventLabel' : undefined,
            'eventValue' : undefined
          });
        });
      }
    });
    $('.galleria-total').text(data_size);
  };

  /**
   * Shows only content in the first visible row
   */
  $.fn.showOnlyNRows = function(options) {
    // Grab the arguments and create variables
    options = $.extend({}, $.fn.showOnlyNRows.defaultOptions, options);
    var param_rows = options['rows'];
    var elements = $(this);
  
    var offset_tops = [];
    var offset_bottoms = [];
    var first_excluded_element;
    var parent = elements.first().parent();
    elements.each(function(index) {
      var elem_top = $(this).offset().top;
      var elem_bottom = elem_top + $(this).outerHeight(true);
      if ($.inArray(elem_top, offset_tops) === -1 && offset_tops.length < param_rows) offset_tops.push(elem_top);
      if ($.inArray(elem_bottom, offset_bottoms) === -1 && offset_bottoms.length < param_rows) offset_bottoms.push(elem_bottom);
      if (($.inArray(elem_top, offset_tops) === -1 && offset_tops.length >= param_rows) && ($.inArray(elem_bottom, offset_bottoms) === -1 && offset_bottoms.length >= param_rows)) {
        first_excluded_element = $(this);
        return false;
      }
    });
    if (typeof first_excluded_element !== "undefined") parent.height(first_excluded_element.offset().top - parent.offset().top).css("overflow", "hidden");
  }
  $.fn.showOnlyNRows.defaultOptions = {
      rows: 1
  };
   
  /**
   * Event Handlers
   */
  // Event handler for when user clicks on any of the sort options
  $('a.sort').on('click', function(evt){
    evt.preventDefault();
    $sort_type = $(this).attr('data-sort-type');
    sortDescending = $(this).attr('data-sort-ascending')==="true";
    
    // Add to datalayer
    dataLayer.push({
      'event' : 'Generic Interaction',
      'eventCategory' : 'Sort Locations',
      'eventAction' : 'Sort ' + ((sortDescending) ? 'descending' : 'ascending'),
      'eventLabel' : $sort_type,
      'eventValue' : undefined
    });
    
    // The sort functions
    
    /**
     * Sort based on city name
     *
     * @param {Object} a 
     * @param {Object} b
     * @returns {Boolean} whether a comes before b
     */
    sort_function_city = function(a,b){
      var an = '';
      var bn = '';
      an = a.getAttribute('data-sort-city').toUpperCase();
      bn = b.getAttribute('data-sort-city').toUpperCase();
      
    	if(an > bn) {
    		return 1;
    	}
    	else if(an < bn) {
    		return -1;
    	}
    	else {
      	return 0;
      }
    };
    
    /**
     * Sort based on country name
     *
     * @param {Object} a 
     * @param {Object} b
     * @returns {Boolean} whether a comes before b
     */
    sort_function_country = function(a,b){
      var an = '';
      var bn = '';
      an = a.getAttribute('data-sort-country').toUpperCase();
      bn = b.getAttribute('data-sort-country').toUpperCase();
      
    	if(an > bn) {
    		return 1;
    	}
    	else if(an < bn) {
    		return -1;
    	}
    	else {
      	sort_function_city(a,b);
    	}
    };
    
    /**
     * Sort based on updated timestamp
     *
     * @param {Object} a 
     * @param {Object} b
     * @returns {Boolean} whether a comes before b
     */
    sort_function_updated = function(a,b){
      var an = '';
      var bn = '';
      an = a.getAttribute('data-sort-updated');
      bn = b.getAttribute('data-sort-updated');
      
    	if(an > bn) {
    		return 1;
    	}
    	else if(an < bn) {
    		return -1;
    	}
    	else {
      	return 0;
    	}
    };
    /**
     * Sort based on added timestamp
     *
     * @param {Object} a 
     * @param {Object} b
     * @returns {Boolean} whether a comes before b
     */  
    sort_function_added = function(a,b){
      var an = '';
      var bn = '';
      an = a.getAttribute('data-sort-added');
      bn = b.getAttribute('data-sort-added');
      
    	if(an > bn) {
    		return 1;
    	}
    	else if(an < bn) {
    		return -1;
    	}
    	else {
      	return 0;
    	}
    };   
    
    // Decide which sort to perform
    switch ($sort_type){
      case "city":
        sortList($('.picasa-locations-list ul')[0], sort_function_city, sortDescending);
        break;
      case "country":
        sortList($('.picasa-locations-list ul')[0], sort_function_country, sortDescending);
        break;
      case "added":
        sortList($('.picasa-locations-list ul')[0], sort_function_added, sortDescending);
        break;
      case "updated":
        sortList($('.picasa-locations-list ul')[0], sort_function_updated, sortDescending);
        break;
    }
    
    // Reset the state for all sort elements
    $('a.sort').attr('data-sort-active','false');
    $('a.sort').attr('data-sort-ascending','false');
    
    // Calculate the new state for the currently chosen sort type
    $(this).attr('data-sort-active','true');
    $(this).attr('data-sort-ascending',!sortDescending);
  });  
  
  
  // Event handlers for when the user clicks on the list of linked
  // locations
  $('.location a').on('click', function(evt) {
    evt.preventDefault();
    var location = $(this).text();
    dataLayer.push({
      'event' : 'Generic Interaction',
      'eventCategory' : 'Location Links',
      'eventAction' : 'Launch Gallery',
      'eventLabel' : location,
      'eventValue' : undefined
    });
    
    launchPhotoGallery(location);
  });
  
  
    
  

  /**
   * Asynchronous loading
   */
  function loadScript() {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAzZqeK3WX7TfndozDuTFkDAwOFbNQOYKY&callback=global.initMap&libraries=geometry";
    document.body.appendChild(script);
  }
  window.onload = loadScript;


});